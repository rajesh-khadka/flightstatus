package com.ekbana.flightstatus.flightstatus.Webservices;

import android.content.Context;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.ekbana.flightstatus.flightstatus.model.Flight;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by root on 1/9/15.
 */
public class JsonData {
    Context context;
    JSONObject object;

    static String URL="http://itoutsourcenepal.com/flight/json-today-flight.php";
    static long expire=15*60*1000;
    public JsonData(Context context)
    {
        this.context=context;
    }

    public void getData()
    {
        AQuery aQuery=new AQuery(context);
        aQuery.ajax(URL, JSONObject.class,expire,new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);

                Log.d("json data",""+object);
                setDataToFlight(object);
            }
        });
    }

    public void setDataToFlight(JSONObject object)
    {
        try {
            ArrayList<Flight>flightArrayList=new ArrayList<>();
            JSONArray jsonArray=object.getJSONArray("arrival");
            Log.d("length of object",""+object.length());
            for(int i=0; i<jsonArray.length();i++) {
                Flight flight=new Flight();
                JSONObject objext1 = jsonArray.getJSONObject(i);

                flight.airLineName=objext1.getString("airline_name");
                Log.d("airLineName",""+flight.airLineName);
                flight.carrierFsCode=objext1.getString("carrier_fs_code");
                flight.flightNumber=objext1.getString("flight_number");
                flight.departureAirportName=objext1.getString("departure_airport_name");
                flight.departureAirportIcao=objext1.getString("departure_airport_icao");
                flight.arrivalAirportName=objext1.getString("arrival_airport_name");
                flight.arrivalAirportIcao=objext1.getString("arrival_airport_icao");


                flightArrayList.add(flight);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
