package com.ekbana.flightstatus.flightstatus.Activity;

import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ExpandableListView;
import android.widget.TextView;


import com.ekbana.flightstatus.flightstatus.R;
import com.ekbana.flightstatus.flightstatus.SlidingTab.SlidingTabsBasicFragment;
import com.ekbana.flightstatus.flightstatus.Webservices.JsonData;

import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends ActionBarActivity{
    Toolbar toolbar;
    ExpandableListView listView;
    ArrayList pList;
    HashMap Clist;
    TextView header;
    Typeface font;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       /* header= (TextView) findViewById(R.id.header_title);
        font=Typeface.createFromAsset(this.getResources().getAssets(),"gold.ttf");
        header.setTextSize(32);
        header.setTypeface(font);*/
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (savedInstanceState == null) {
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            SlidingTabsBasicFragment fragment = new SlidingTabsBasicFragment();
            transaction.replace(R.id.sample_content_fragment, fragment);
            transaction.commit();
        }

        JsonData jsonData=new JsonData(this);
        jsonData.getData();



    }


   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

}
