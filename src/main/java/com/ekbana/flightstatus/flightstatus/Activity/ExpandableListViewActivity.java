package com.ekbana.flightstatus.flightstatus.Activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ExpandableListView;

import com.ekbana.flightstatus.flightstatus.Adapter.ExpandableListViewAdapter;
import com.ekbana.flightstatus.flightstatus.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 1/8/15.
 */
public class ExpandableListViewActivity extends ActionBarActivity {
    ExpandableListView listView;
    ArrayList pList;
    HashMap Clist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expandable_list);

        listView= (ExpandableListView) findViewById(R.id.lvExp);
        pList = new ArrayList<String>();
        Clist = new HashMap<String, ArrayList<String>>();

        pList.add("EK Notice");
        pList.add("Data management");
        pList.add("Categories");
        pList.add("final");


      /*  List<String> a=new ArrayList<String>();
        a.add("hello");
        a.add("world");
        a.add("java");
        a.add("need");
        a.add("code");
        a.add("atoz");

        List<String>b=new ArrayList<String>();
        b.add("social");
        b.add("media");
        b.add("youtube");
        b.add("skype");
        b.add("cvg");
        b.add("lpt");


        List<String>c=new ArrayList<String>();
        c.add("social");
        c.add("yello");
        c.add("sudo");
        c.add("apt");
        c.add("get");

        Clist.put(pList.get(0),a);
        Clist.put(pList.get(1),b);
        Clist.put(pList.get(2),c);
        Clist.put(pList.get(3),new ArrayList<String>());*/



        ExpandableListViewAdapter adapter=new ExpandableListViewAdapter(this,pList);
        listView.setAdapter(adapter);
        //setGroupIndicatorToRight();



    }
}
