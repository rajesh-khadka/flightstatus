package com.ekbana.flightstatus.flightstatus.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.ekbana.flightstatus.flightstatus.R;

import java.util.List;

/**
 * Created by root on 12/30/14.
 */
public class ExpandableListViewAdapter extends BaseExpandableListAdapter {

    List<String> Plist;
    //HashMap<String, List<String>> Clist;
    Context context;

    public ExpandableListViewAdapter(Context c, List<String> Plist) {
        this.Plist = Plist;
        this.context = c;

    }


    @Override
    public int getGroupCount() {
        return Plist.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return 1;


    }

    @Override
    public Object getGroup(int groupPosition) {
        return Plist.get(groupPosition);
    }

    @Override
    public String getChild(int groupPosition, int childPosition) {
        return "sucess";
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View prow=convertView;
        String headerTitle = (String) getGroup(groupPosition);
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            prow = layoutInflater.inflate(R.layout.parent_layout, parent, false);

        /*TextView textView = (TextView) prow.findViewById(R.id.ptextView);
        textView.setText(headerTitle);
        Log.d("header", "" + headerTitle);
*/
        return prow;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View crow = layoutInflater.inflate(R.layout.child_layout, parent, false);

        /*TextView textView= (TextView) crow.findViewById(R.id.ctextView);
        textView.setText("child");*/
        return crow;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
