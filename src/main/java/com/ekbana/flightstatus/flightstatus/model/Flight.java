package com.ekbana.flightstatus.flightstatus.model;

/**
 * Created by root on 1/9/15.
 */
public class Flight {
    public String airLineName;
    public String carrierFsCode;
    public String flightNumber;
    public String departureAirportName;
    public String departureAirportIcao;
    public String departureTime;
    public String departureDate;
    public String arrivalAirportName;
    public String arrivalAirportIcao;
    public String arrivalTime;
    public String arrivalDate;


}
